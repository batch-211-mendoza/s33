// JAVASCRIPT SYNCHRONOUS AND ASYNCHRONOUS
/*
	Synchronous
	- Synchronous codes run in sequence. This means that each operation must wait for the previous one to complete before executing.

	Asynchronous
	- Asynchronous code run in parallel. This means that an operation can occur while another one is still being processed.

	Asynchronous code execution is often preferrable in situations where execution can be blocked. Some examples of this are network requests, long-running calculations, file system operations, etc. Using asynchronous code in the browser ensures the page remains responsive and the user experience is mostly unaffected.
*/
// Example:
	console.log("Hello World");
	// cosnole.log("Hello Again");

	// for (let i = 0; i <= 10000; i++){
	// 	console.log(i);
	// }

	// The browser will wait first for the for loop to finish before running the next line of code
	/*console.log("Hello it's me");*/

/*
	- API stands for Application Programming Language
	- An application programming language is a particular set of codes that allow software programs to communicate with each other
	- An API is the interface through which you access someone else's code or through which someone else's code accesses yours.

	Example:
		Google APIs
			https://developers.google.com/identity/sign-in/web/sign-in
		Youtube API
			https://developers.google.com/youtube/iframe_api_reference
*/

// FETCH API

	/*console.log(fetch("https://jsonplaceholder.typicode.com/posts"));*/
	//output: promise

/*
	A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

	A promise is in one of these three states:
	• Pending:
		- Initial state, neither fulfilled nor rejected
	• Fulfilled:
		- Operation was successfully completed
	• Rejected:
		- Operation failed
*/

/*
	- By using the .then mothod, we can now check for the status of the promise
	- The "fetch" method will return a "promise" that resolves to be a "response" object
	- The ".then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected"

	Syntax:
		fetch("<url>").then(response => {});
*/

	/*fetch("https://jsonplaceholder.typicode.com/posts").then(response => console.log(response.status));*/

	/* // Use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application
	fetch("https://jsonplaceholder.typicode.com/posts")
	// Print the converted JSON value fromt he "fetch" request
	.then((response) => response.json())
	// Using multiple ".then" method to create a promise chain
	.then((json) => console.log(json)); */

/*
	- The "async" and "await" keywords is another approach that can be used to achieve asynschronous codes
	- Used in functions to indicate which portions of code should be waited
	- Creates an asynchronous function
*/

	async function fetchData(){
		
		// Waits for the "fetch" method to complete then stores the value in the "result" variable
		let result = await fetch("https://jsonplaceholder.typicode.com/posts")
		
		// Result returned by fetch is returned as a promise
		console.log(result);

		// The returned "response" is an object
		console.log(typeof result);

		// We cannot access the content of the "response" by directly accessing its body property
		console.log(result.body);
		let json = await result.json();
		console.log(json);
	}
	// fetchData();

// GETTING A SPECIFIC POST
/*
	Retrieves a specific post following the REST API (retrieve, /posts/:id, GET)
*/

	/*fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then((response) => response.json())
	.then((json) => console.log(json));*/

/*
	Postman

	url: https://jsonplaceholder.typicode.com/posts/1
	method: GET
*/

// CREATING A POST
/*
	Syntax:
		fetch("<url>", options)
		.then((response) => {})
		.then((response) => {});
*/

	// Creates a new post following the REST API (create, /posts/:id, POST)
	/*fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		headers: 
		{
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "My First Blog Post",
			body: "Hello World",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));*/

/*
	POSTMAN

	url: https://jsonplaceholder.typicode.com/posts
	method: POST
	body: raw + JSON
		{
			"title" : "My First Blog Post",
			"body": "Hello World",
			"userId": 1
		}
*/

// UPDATING A POST USING THE PUT METHOD
/*
	Updates a specific post following the REST API (update, /posts/:id, PUT)
*/
	/*fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			id: 1,
			title: "Updated post",
			body: "Hello again!",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));*/

/*
	POSTMAN

	url: https://jsonplaceholder.typicode.com/posts/1
	method: PUT
	body: raw + json
		{
			"title": "My First Revised Blog Post",
			"body": "Hello there! I revised this a bit",
			"userId": 1
		}

*/

// UPDATING A POST USING THE PATCH METHOD
/*
	- Updates a specific post following the REST API (update, /posts/:id, PATCH)
	- THe differences between PUT and PATCH is the number of properties being changed
	- PATCH updates parts of the document
	- PUT udpates the whole document
*/

	/*fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "Corrected post"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));*/

/*
	POSTMAN

	url: https://jsonplaceholder.typicode.com/posts/1
	method: PATCH
	body: raw + JSON
		{
			"title": "This is my final title"
		}
*/

// DELETING A POST
/*
	Deleting a specific post following the REST API (delete, /posts/:id, DELETE)
*/

	/*fetch("https://jsonplaceholder.typicode.com/posts/1", {
			method: "DELETE",
		})
		.then((response) => response.json())
		.then((json) => console.log(json));*/

/*
	POSTMAN

	url: https://jsonplaceholder.typicode.com/posts/1
	method: DELETE
*/

// FILTERING THE POST
/*
	- The data can be filtered by sending the userId along with the URL
	- Information sent via the URL can be done by adding the question mark symbol ( ? )

	Syntax:
		Individual Parameter:
			"url?parameterName=value"
		Multiple Parameter:
			"url?paramA=valueA&paramB=valueB"
*/
	
		/*fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
		.then((response) => response.json())
		.then((json) => console.log(json));*/

		/*fetch("https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3")
		.then((response) => response.json())
		.then((json) => console.log(json));*/

// RETRIEVE COMMENTS OF A SPECIFIC POST
/*
	Retrieving comments for a specific post following the REST API (retrieve, /posts/id:, GET)
*/
	
	fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
	.then((response) => response.json())
	.then((json) => console.log(json));